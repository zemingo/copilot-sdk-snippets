import { Injectable } from '@angular/core';
import * as firebase from 'nativescript-plugin-firebase';
import { LogEventParameter } from 'nativescript-plugin-firebase/analytics/analytics';

@Injectable()
export class CoPilotService {

    public fba: any;
    public userId: any;
    public userEmail: any;
    public signupMethod: any;

    private EVENT_ORIGIN: string = "event_origin";
    private EVENT_ORIGIN_SERVER: string = "Server";
    private EVENT_ORIGIN_USER: string = "User";
    private EVENT_ORIGIN_APP: string = "App";
    private EVENT_ORIGIN_THING: string = "Thing";

    private USER_ID: string = "user_id";
    private USER_EMAIL: string = "user_email";
    private SIGNUP_METHOD: string = "sign_up_method";
    private SCREEN_NAME: string = "screen_name";

    constructor() { }

    /**
     * Enable or disable analytics collection per user's GDPR consent
     */
    public enableCollection(enable: any) {
        firebase.analytics.setAnalyticsCollectionEnabled(enable);
    }

    /**
     * Call this method when the logs in.
     */
    public userLoggedIn(userId: any, email: any) {
        this.userId = userId;
        this.userEmail = email;
        this.signupMethod = "Email";
        firebase.analytics.setUserId({ userId: userId });
    }

    /**
     * Call this method when the logges in (or registers) anonymously.
     */
    public userLoggedInAnonymous(userId: any) {
        this.userId = userId;
        this.userEmail = null;
        this.signupMethod = "Anonymous";
        firebase.analytics.setUserId({ userId: userId });
    }

    /**
     * Call this method when the user logs out in order to clear state
     */
    public userLoggedout() {
        this.userId = null;
        this.userEmail = null;
        this.signupMethod = null;
        firebase.analytics.setUserId({ userId: null });
        //firebase.analytics.resetAnalyticsData();
    }

    public createGeneralParameters(origin: any) {
        return [
            { key: this.USER_ID, value: this.userId },
            { key: this.USER_EMAIL, value: this.userEmail },
            { key: this.SIGNUP_METHOD, value: this.signupMethod },
            { key: this.EVENT_ORIGIN, value: origin }
        ];
    }

    public internalLog(eventName: string, params: Array<LogEventParameter>) {
        firebase.analytics.logEvent({ key: eventName, parameters: params })
            .then((res) => { console.log(res); })
            .catch((error) => console.error(error));
    }

    public logScreenLoad(screenName: any) {
        const params = [
            { key: this.SCREEN_NAME, value: screenName },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_USER)
        ]

        this.internalLog("screen_load", params)
    }

    public logTapMenu(screenName: any) {
        const params = [
            { key: this.SCREEN_NAME, value: screenName },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_USER)
        ]

        this.internalLog("tap_menu", params)
    }

    public logTapMenuItem(screenName: any, menuItem: any) {
        const params = [
            { key: this.SCREEN_NAME, value: screenName },
            { key: "menu_item", value: menuItem },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_USER)
        ]

        this.internalLog("tap_menu_item", params)
    }

    public logFirmwareUpgradeStarted() {
        const params = this.createGeneralParameters(this.EVENT_ORIGIN_APP)
        this.internalLog("firmware_upgrade_started", params)
    }

    public logFirmwareUpgradeComplet(wasSuccesful: any) {
        const params = [
            { key: "status", value: wasSuccesful ? "Success" : "Failure" },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_APP)
        ]
        this.internalLog("firmware_upgrade_complete", params)
    }

    public logErrorReport(screenName: any, errorType: any) {
        const params = [
            { key: this.SCREEN_NAME, value: screenName },
            { key: "error_type", value: errorType },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_SERVER)
        ]

        this.internalLog("error_report", params)
    }

    public logSignup() {
        const params = [
            { key: this.SCREEN_NAME, value: "sign_up" },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_SERVER)
        ]
        this.internalLog("sign_up", params)
    }

    public logSuccesfulEvelevateAnonymous() {
        const params = [
            { key: this.SCREEN_NAME, value: "sign_up" },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_SERVER)
        ]
        this.internalLog("successful_elevate_anonymous", params)
    }

    public logLogin() {
        const params = [
            { key: this.SCREEN_NAME, value: "login" },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_SERVER)
        ]

        this.internalLog("login", params)
    }

    public logLogout() {
        const params = [
            { key: this.SCREEN_NAME, value: "settings" },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_USER)
        ]
        this.internalLog("logout", params)
    }

    public logAcceptTerms(termsOfuseVersion: any) {
        const params = [
            { key: this.SCREEN_NAME, value: "accept_terms" },
            { key: "version", value: termsOfuseVersion },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_USER)
        ]

        this.internalLog("accept_terms", params)
    }

    public logOnboardingStarted(screenName: any) {
        const params = [
            { key: this.SCREEN_NAME, value: screenName },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_APP)
        ]

        this.internalLog("onboarding_started", params)
    }

    public logOnboardingEnded(screenName: any) {
        const params = [
            { key: this.SCREEN_NAME, value: screenName },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_APP)
        ]

        this.internalLog("onboarding_ended", params)
    }

    public logTapConnectDevice() {
        const params = {
            ...
            this.createGeneralParameters(this.EVENT_ORIGIN_USER),
        }
        this.internalLog("onboarding_ended", params)
    }

    public logThingFound(thingId: any) {
        const params = [
            { key: "thing_id", value: thingId },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_APP)
        ]

        this.internalLog("thing_found", params)
    }

    public logThingConnected(thingId: any, screenName: any) {
        const params = [
            { key: "thing_id", value: thingId },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_THING)
        ]

        this.internalLog("thing_connected", params)
    }

    public logThingInfo(firmware: any, model: any, thingPhysicalId: any) {
        const params = [
            { key: "thing_model", value: model },
            { key: "thing_firmware", value: firmware },
            { key: "thing_id", value: thingPhysicalId },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_THING)
        ]

        this.internalLog("thing_info", params)
    }

    public logThingConnectionFailed(failureReason: any) {
        const params = [
            { key: "failure_reason", value: failureReason },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_APP)
        ]

        this.internalLog("thing_connection_failed", params)
    }

    public logConsumableUsage(consumableType: any, thingPhysicalId: any) {
        const params = [
            { key: "consumable_type", value: consumableType },
            { key: "thing_id", value: thingPhysicalId },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_APP)
        ]
        this.internalLog("consumable_usage", params)
    }
    
    public logContactSupport(supportCase: any, thingPhysicalId: any, screeName: any) {
        const params = [
            { key: "support_case", value: supportCase },
            { key: "thing_id", value: thingPhysicalId },
            { key: this.SCREEN_NAME, value: screenName },
            ...this.createGeneralParameters(this.EVENT_ORIGIN_USER)
        ]
        this.internalLog("contact_support", params)
    }
}
