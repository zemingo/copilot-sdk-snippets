import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.copilot.analytics.AnalyticsConstants;
import com.copilot.analytics.EventLogProvider;
import com.copilot.analytics.Grouping;
import com.copilot.analytics.providers.ProviderGroup;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Map;

// Please add    classpath 'com.google.gms:google-services:x.x.x' to your main gradle file and
// Please add    apply plugin: 'com.google.gms.google-services' to your app gradle file

@Grouping(groups = {ProviderGroup.All})
public class FirebaseAnalyticsEventLogProvider implements EventLogProvider {

    private static final String TAG = FirebaseAnalyticsEventLogProvider.class.getSimpleName();

    @NonNull
    private final Context mContext;

    public FirebaseAnalyticsEventLogProvider(@NonNull Context context) {
        mContext = context.getApplicationContext();
        deactivate();
    }

    @Override
    public String getProviderName() {
        return TAG;
    }

    @Override
    public void activate() {
        FirebaseAnalytics.getInstance(mContext).setAnalyticsCollectionEnabled(true);
    }

    @Override
    public void deactivate() {
        FirebaseAnalytics.getInstance(mContext).setAnalyticsCollectionEnabled(false);
    }

    @Override
    public Map<String, String> transformParameters(Map<String, String> parameters) {
        return parameters;
    }

    @Override
    public void logEvent(@NonNull String eventName, Map<String, String> transformedParams) {
        Bundle bundle = buildEventBundle(transformedParams);
        FirebaseAnalytics.getInstance(mContext).logEvent(eventName, bundle);
    }

    @Override
    public void logScreenLoadEvent(@NonNull String screenName, Map<String, String> transformedParams) {
        Bundle bundle = buildEventBundle(transformedParams);
        bundle.putString(AnalyticsConstants.SCREEN_NAME, screenName);
        FirebaseAnalytics.getInstance(mContext).logEvent(AnalyticsConstants.SCREEN_LOAD_EVENT, bundle);
    }

    private Bundle buildEventBundle(Map<String, String> parameters) {
        Bundle params = new Bundle();

        for (Map.Entry<String, String> parameter : parameters.entrySet()) {
            String key = parameter.getKey();
            String value = parameter.getValue();
            params.putString(key, value);
        }
        return params;
    }
}
