import android.support.annotation.NonNull;
import android.util.Log;

import com.copilot.analytics.EventLogProvider;
import com.copilot.analytics.Grouping;
import com.copilot.analytics.providers.ProviderGroup;

import java.util.Map;


@Grouping(groups = {ProviderGroup.Main, ProviderGroup.All})
public class DebugLogsEventLogProvider implements EventLogProvider {

    private static final String TAG = DebugLogsEventLogProvider.class.getSimpleName();

    @Override
    public String getProviderName() {
        return DebugLogsEventLogProvider.class.getSimpleName();
    }

    @Override
    public void activate() {
        //Nothing to do here
    }

    @Override
    public void deactivate() {
        //Nothing to do here
    }

    @Override
    public Map<String, String> transformParameters(Map<String, String> parameters) {
        return parameters;
    }

    @Override
    public void logEvent(@NonNull String eventName, Map<String, String> transformedParams) {
        Log.v(TAG, String.format("logEvent - Event name = <%s>", eventName));

        for (Map.Entry<String, String> entry : transformedParams.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.v(TAG, String.format("key = <%s>,  value = <%s>", key, value));
        }
    }

    @Override
    public void logScreenLoadEvent(@NonNull String screenName, Map<String, String> transformedParams) {
        Log.v(TAG, String.format("logScreenLoadEvent - Event name = <%s>", screenName));
        for (Map.Entry<String, String> entry : transformedParams.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.v(TAG, String.format("key = <%s>, value = <%s>", key, value));
        }
    }
}