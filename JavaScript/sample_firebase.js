
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

const EVENT_ORIGIN = "event_origin"
const EVENT_ORIGIN_SERVER = "Server"
const EVENT_ORIGIN_USER = "User"
const EVENT_ORIGIN_APP = "App"

const USER_ID = "user_id"
const USER_EMAIL = "user_email"
const SIGNUP_METHOD = "sign_up_method"
const SCREEN_NAME = "screen_name"

/** 
 * This file describes JS integration for ionic.
 * Please replace implementation of `fba` for your Firebase analytics reporting logic. 
 * IONIC plugin https://ionicframework.com/docs/native/firebase-analytics
 * Native script : https://github.com/eddyverbruggen/nativescript-plugin-firebase/blob/HEAD/docs/ANALYTICS.md
*/
export class EventLoggerProvider {

    constructor(fba) {
        // TODO - Initialize the Firebase events collection
        this.fba = fba
    }

    /**
     * Enable or disable analytics collection per user's GDPR consent
     */
    enableCollection = (enable) => {
        // TODO - replace this.fba with your firebase implementation
        this.fba.setEnabled(enable);
    }

    clearAnalyticsSession = () =>{
        // TODO - replace this.fba with your `resetAnalyticsData` firebase implementation
        this.fba.resetAnalyticsData();
    }

    internalLog = (eventName, params) => {
        // TODO - replace this logic with your reporting firebase implementation logic
        this.fba.logEvent(eventName, params)
            .then((res) => { console.log(res); })
            .catch((error) => console.error(error));
    }

    internalSetUserId = (userId) => {
        // TODO - replace this method with your firebase setUserId method.
        this.fba.setUserId(useId);
    }

    /**
     * Call this method when the logs in.
     */
    userLoggedIn = (userId, email) => {
        this.userId = userId;
        this.userEmail = email;
        this.signupMethod = "Email";
        this.internalSetUserId(userId);
    }

    /**
     * Call this method when the logs-in (or registers) anonymously.
     */
    userLoggedInAnonymous = (userId) => {
        this.userId = userId;
        this.userEmail = null;
        this.signupMethod = "Anonymous";
        this.internalSetUserId(userId);
    }

    /**
     * Call this method when the user logs out in order to clear state
     */
    userLoggedOut = () => {
        this.userId = null;
        this.userEmail = null;
        this.signupMethod = null;
        this.internalSetUserId(null);
        this.clearAnalyticsSession();
    }

    createGeneralParameters = (origin) => {
        return {
            [USER_ID]: userId,
            [USER_EMAIL]: userEmail,
            [SIGNUP_METHOD]: signupMethod,
            [EVENT_ORIGIN]: origin
        };
    }

    logScreenLoad = (screenName) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_USER),
            [SCREEN_NAME]: screenName
        }

        this.internalLog("screen_load", params)
    }

    logTapMenu = (screenName) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_USER),
            [SCREEN_NAME]: screenName
        }

        this.internalLog("tap_menu", params)
    }

    logTapMenuItem = (screenName, menuItem) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_USER),
            [SCREEN_NAME]: screenName,
            "menu_item": menuItem
        }

        this.internalLog("tap_menu_item", params)
    }

    logFirmwareUpgradeStarted = () => {
        const params = createGeneralParameters(EVENT_ORIGIN_APP)
        this.internalLog("firmware_upgrade_started", params)
    }

    logFirmwareUpgradeComplete = (wasSuccessful) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_APP),
            "status": (wasSuccessful ? "Success" : "Failure")
        }
        this.internalLog("firmware_upgrade_complete", params)
    }

    logErrorReport = (screenName, errorType) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_SERVER),
            [SCREEN_NAME]: screenName,
            "error_type": errorType
        }

        this.internalLog("error_report", params)
    }

    logSignup = () => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_SERVER),
            [SCREEN_NAME]: "sign_up"
        }
        this.internalLog("sign_up", params)
    }

    logSuccessfulElevateAnonymous = () => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_SERVER),
            [SCREEN_NAME]: "sign_up"
        }
        this.internalLog("successful_elevate_anonymous", params)
    }

    logLogin = () => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_SERVER),
            [SCREEN_NAME]: "login"
        }
        this.internalLog("login", params)
    }

    logLogout = () =>{
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_USER),
            [SCREEN_NAME]: "settings"
        }
        this.internalLog("logout", params)
    }

    logAcceptTerms = (termsOfUseVersion) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_USER),
            [SCREEN_NAME]: "accept_terms",
            "version": termsOfUseVersion
        }
        this.internalLog("accept_terms", params)
    }

    logOnboardingStarted = (screenName) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_APP),
            [SCREEN_NAME]: screenName
        }
        this.internalLog("onboarding_started", params)
    }

    logOnboardingEnded = (screenName) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_APP),
            [SCREEN_NAME]: screenName
        }
        this.internalLog("onboarding_ended", params)
    }

    logTapConnectDevice = () => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_USER),
        }
        this.internalLog("onboarding_ended", params)
    }

    logThingFound = (thingId) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_APP),
            "thing_id": thingId
        }
        this.internalLog("thing_found", params)
    }

    logThingConnected = (thingId, screenName) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_THING),
            "thing_id": thingId
        }
        this.internalLog("thing_connected", params)
    }

    logThingInfo = (firmware, model, thingPhysicalId) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_THING),
            "thing_firmware": firmware,
            "thing_model": model,
            "thing_id": thingPhysicalId
        }
        this.internalLog("thing_info", params)
    }

    logThingConnectionFailed = (failureReason) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_APP),
            "failure_reason": failureReason
        }
        this.internalLog("thing_connection_failed", params)
    }

    logConsumableUsage = (consumableType, thingPhysicalId) => {
        const params = {
            ...
            createGeneralParameters(EVENT_ORIGIN_THING),
            "consumable_type": consumableType,
            "thing_id": thingPhysicalId
        }
        this.internalLog("consumable_usage", params)
    }
}