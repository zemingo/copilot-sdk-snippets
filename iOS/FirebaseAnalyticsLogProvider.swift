import Foundation
import Firebase
import FirebaseAnalytics
import CopilotAPIAccess

class FirebaseAnalyticsProvider: EventLogProvider {

    func enable() {
        updateFirebaseAnalyticsStatus(shouldLogEvents: true)
    }

    func disable() {
        updateFirebaseAnalyticsStatus(shouldLogEvents: false)
    }

    init() {
        //App delegate runs firebase configure
        updateFirebaseAnalyticsStatus(shouldLogEvents: false)
    }

    private func updateFirebaseAnalyticsStatus(shouldLogEvents: Bool) {
        AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(shouldLogEvents)
    }

    func transformParameters(parameters: Dictionary<String, String>) -> Dictionary<String, String> {
        return parameters
    }

    func logCustomEvent(eventName: String, transformedParams: Dictionary<String, String>) {
        Analytics.logEvent(eventName, parameters: transformedParams)
    }

    func logScreenLoadEvent(screenName: String, transformedParams: Dictionary<String, String>) {

        var eventParams = transformedParams
        eventParams.updateValue(screenName, forKey: AnalyticsConstants.screenNameKey)

        Analytics.logEvent(AnalyticsConstants.screenLoadEventName, parameters: eventParams)
    }

    var providerName: String {
        return "FirebaseAnalyticsProvider"
    }

    var providerEventGroups: [AnalyticsEventGroup] {
        return [.All]
    }
}
